#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
def stageStep(num):

    if num==1:
        return 1
    else:
        return 2**(num-1)

n = float(sys.argv[1])
print(stageStep(n))


# In[ ]:




