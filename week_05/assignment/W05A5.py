#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import sys
num = int(sys.argv[1])

j=2
prime=True

while j<num**0.5:
    if (num%j==0):
        prime = False
        break
    j+=1
if prime:
    print(num,'is a prime')
else:
    print(num,'isn\'t a prime.')

