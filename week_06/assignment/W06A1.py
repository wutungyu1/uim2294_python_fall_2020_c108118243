#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
def factorial_loop (n):
    '''
        while階層計算
    '''
    factor = 1
    i = 1
    while i <= n:
        factor *= i
        i+=1
    return factor
n = int(sys.argv[1])
print(factorial_loop(n))


# In[ ]:




