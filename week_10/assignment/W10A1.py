#!/usr/bin/env python
# coding: utf-8

# In[3]:


import matplotlib as mpl
import matplotlib.pyplot as plt
import random
x=1
y=1
n=2000

data=[random.random() for _ in range(n)]
for i in data:
    x,y=random.random(),random.random()
    if(((x**2)+(y**2))**0.5<=1.0):

        plt.plot(x,y,color="blue",marker=".")
    else:
        plt.plot(x,y,color="red",marker=".")

plt.grid(True)


# In[ ]:




