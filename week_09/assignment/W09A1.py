#!/usr/bin/env python
# coding: utf-8

# In[23]:


import pandas as pd


data = {
"Stranger Things":25,
"The Umbrella Academy":165,
"Daredevil":12,
"The Witcher":3,
"Ozark":96,
"Black Mirror":453,
"Jessica Jones":35,
"Punisher":65,
"The Crown":29,
"Mindhunter":73,
}

def MN_Demo(data):
    '''
    python最大值、最小值
    '''
    Min=data.idxmin()
    Max=data.idxmax()
    return Max,Min
    

df=pd.DataFrame.from_dict(data,orient='index')
MN_Demo(df)


# In[ ]:




