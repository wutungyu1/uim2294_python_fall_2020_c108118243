#!/usr/bin/env python
# coding: utf-8

# In[10]:


import numpy as np
import numpy.random as nr
import sys
n=int(sys.argv[1])

def Numpy_demo(size):
    '''
    最大值、最小值、平均數
    '''
    value=nr.rand(size)
    Max_N=np.max(value)
    Min_N=np.min(value)
    Mean_N=np.mean(value)
    return Max_N,Min_N,Mean_N

print(Numpy_demo(n))


# In[ ]:




