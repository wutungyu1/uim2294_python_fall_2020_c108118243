#!/usr/bin/env python
# coding: utf-8

# In[18]:


import statistics as st
import random
import sys
n=int(sys.argv[1])
def MMA_Demo(size):
    '''
    最大值、最小值、平均數
    '''
    value=[random.random() for _ in range(n)]
    Max_N=max(value)
    Min_N=min(value)
    Mean_N=st.mean(value)
    return Max_N,Min_N,Mean_N

print(MMA_Demo(n))


# In[ ]:




